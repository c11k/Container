<?php
/**
 * PHP Version 7.1
 *
 * @package c11k/container
 */

declare(strict_types=1);

namespace C11K\Container\Reference;

abstract class AbstractReference
{
    private $name;

    /**
     * AbstractReference constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
