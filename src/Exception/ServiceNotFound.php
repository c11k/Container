<?php
/**
 * PHP Version 7.1
 *
 * @package c11k/container
 * Throws Psr\Container\NotFoundExceptionInterface
 */

declare(strict_types=1);

namespace C11K\Container\Exception;

use Psr\Container\NotFoundExceptionInterface;

class ServiceNotFound extends \Exception implements NotFoundExceptionInterface
{

}
