<?php
/**
 * PHP Version 7.1
 *
 * @package c11k/container
 * Throws ContainerException
 */

declare(strict_types=1);

namespace C11K\Container\Exception;

class ParameterNotFound extends ContainerException
{

}
