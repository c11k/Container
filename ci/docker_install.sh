#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq
apt-get install \
    git \
    wget \
    zlib1g-dev \
    -yqq

docker-php-ext-install zip

wget https://raw.githubusercontent.com/composer/getcomposer.org/eb2dd7a54a41891e27ab30e59b79782648380e89/web/installer -O - -q | php -- --quiet --install-dir=/usr/local/bin --filename=composer
