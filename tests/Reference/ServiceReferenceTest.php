<?php

namespace C11K\Container\Tests\Reference;

use C11K\Container\Reference\ServiceReference;
use PHPUnit\Framework\TestCase;

class ServiceReferenceTest extends TestCase
{
    public function testServiceReference()
    {
        $reference = new ServiceReference('foo.bar');
        $this->assertEquals('foo.bar', $reference->getName());
    }
}
